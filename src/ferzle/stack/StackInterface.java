package ferzle.stack;
import java.util.Iterator;
/**
 * A simple Stack interface
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */

public interface StackInterface<T>
{
    public boolean push(T item);
    public T pop();
    public T peek();
    
    public boolean isEmpty();
    public boolean isFull();
    public Iterator<T> iterator();
}
