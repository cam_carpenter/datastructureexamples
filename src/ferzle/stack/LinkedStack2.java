package ferzle.stack;
import java.util.Iterator;

import ferzle.util.Node;
import ferzle.util.NodeIterator;

/**
 * A simple Stack implementation based on a linked list
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class LinkedStack2<T> implements StackInterface<T>
{
    Node<T> top;

    /**
     * Constructor for objects of class LinkedStack
     */
    public LinkedStack2()
    {
        top=null;
    }

    public boolean isEmpty() {
        return top==null;
    }
    
    public boolean isFull() {
        return false;
    }
    
    public boolean push(T item) {
        Node<T> newTop = new Node<T>(item);
        newTop.setNext(top);
        top = newTop;
        return true;
    }
    public T pop() {
        if(isEmpty()) {
            return null;
        } else {
            T key = top.getKey();
            top = top.getNext();
            return key;
        }
    } 
    public T peek() {
        if(isEmpty()) {
            return null;
        } else {
            return top.getKey();
        }
    }
    public Iterator<T> iterator() {
        return new NodeIterator(top);
    }
}
