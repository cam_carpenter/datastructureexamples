package ferzle.queue;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A simple queue class based on an ArrayList
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class ArrayListQueue<T> implements QueueInterface<T> {
	
	// The data
   ArrayList<T> theElements;
   
    public ArrayListQueue() {
        theElements = new ArrayList<T>();
    }

	@Override
    public boolean isEmpty() {
    	// TODO Implement me!
    	return false;
    }

	@Override
    public boolean isFull() {
    	// TODO Implement me!
    	return false;
    }

	@Override
    public boolean enqueue(T item) {
    	// TODO Implement me!
    	return false;
    }

	@Override
    public T dequeue() {
    	// TODO Implement me!
    	return null;
    } 

	@Override
    public T peek() {
    	// TODO Implement me!
    	return null;
    }

	@Override
	public Iterator<T> iterator() {
		// TODO Implement me if time permits.
		return null;
	}
}
